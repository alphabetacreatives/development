#!/bin/bash
#This will login to the live person CLI.  
#The below credentials are just a sample, you will need to add your own from Liveerson.

#Preferbably you will use the SSO method, but both login methods will work. 

#I recommend using a docker secret or third party service for your creds for additional secruity layer

#Normal Login
lpf login -a 123456789 -u user@liveperson.com -p p4ssw0rd


#SSO Login (Uncomment the below to usee SSO and comment out above Login)
#lpf login --token <bearer> --accountId <accountId> --userId <userId>


#exec "@"
