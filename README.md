# Liveperson CLI Dockerfile

[Docker](http://docker.com) container to use [LivePerson CLI](https://www.npmjs.com/package/liveperson-functions-cli#init).


## Usage

### Install

Build `Dockerfile Image` from source:

    git clone https://gitlab.com/alphabetacreatives/development.git
    cd development (change the folder name if desired)
    Open the .sh script and add your credentials.  You can use the direct login, SSO, or a Docker Secret.
    docker build -t (Your Image Name) .

### Run

Run the image to enter and keep alive container:

    docker run -it <image-name-or-id> (Logs Into LP Automatically and then exits)
    docker run -it --entrypoint bash <image-name-or-id>  (Logs into the command bash prompt)

You will get the "WELCOME TO LIVEPERSON FUNCTIONS" message at the bash command prompt. You can run any LP command (ie.  lpf login -h).