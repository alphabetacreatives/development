

#Download base image ubuntu 18.04.  We are using LTS
FROM ubuntu:18.04

# Update the Ubuntu Software repository
RUN apt-get update

#Image Authhor
LABEL maintainer="marketing@alphabetacreatives.com"

#Install Curl so we can utilize this command later
RUN apt-get install curl wget -y

#Create a Node Version Manager Directory
#RUN mkdir $NVM_DIR

#Set the environment variables for both Node and the Node version manager 
ENV NVM_VERSION v0.33.11
ENV NODE_VERSION v14.0.0
ENV NVM_DIR /usr/local/nvm
RUN mkdir $NVM_DIR

#Pull the node version manager script from the source
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

#Set the Paths Node Version Manager installation.
ENV NODE_PATH $NVM_DIR/$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH

#Finally we'll install Node and NVM
RUN echo "source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default"

RUN curl -L https://www.npmjs.com/install.sh | sh

# Install Liveperson Functions

RUN yes | npm i liveperson-functions-cli -g

# Copy the entrypoint shell script into the container for later use when the container starts up.

COPY lpc_start.sh /lpcommand/lpc_start.sh

# Make the Shell script executable
RUN chmod +x /lpcommand/lpc_start.sh

#This first volume is where we'll set the credentials.  LP CLI looks for them in root.
VOLUME /root/.lpcli

#We create the volume and the working directory.  It is where the cli commands will be run.
VOLUME /lpcommand
WORKDIR /lpcommand

#We'll set the entrypoint which sets the command that will run when the conatiner is executed
ENTRYPOINT ["/lpcommand/lpc_start.sh"]
